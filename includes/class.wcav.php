<?php
class WCAV {
    public function __construct() {
        /**
         * Add Acction: Add Custom fields to the WooCommerce checkout
         */
        add_action( 'woocommerce_after_checkout_billing_form', array($this, prefix.'custom_checkout_field') );
        
        /**
         * Add Acction: Validate Custom Fileds and Verify Age through API.
         */
        add_action('woocommerce_checkout_process', array($this, prefix.'verify_age_validate'));
    }
    
    /**
     * Function: Add Acction: Add Custom fields to the WooCommerce checkout
     */
    public function wcav_custom_checkout_field($checkout){
        woocommerce_form_field( 'dob', array(
            'type'          => 'text',
            'custom_attributes' => array('item-description'=>'Format: 1950-01-31 (YYYY-MM-DD)'),
            'input_class' => array('append-description'),
            'class'         => array('my-field-class form-row-wide'),
            'label'         => __('Date of Birth'),
            'placeholder'   => __('Enter Date of Birth'),
            'required'          => true,
            ), $checkout->get_value( 'dob' ));
        
        woocommerce_form_field( 'SSN', array(
            'type'          => 'text',
            'custom_attributes' => array('item-description'=>'Last 4 digits of SSN'),
            'input_class' => array('append-description'),
            'class'         => array('my-field-class form-row-wide'),
            'label'         => __('SSN#'),
            'placeholder'   => __('Enter SSN#'),
            'required'          => true,
            ), $checkout->get_value( 'SSN' ));
        ?>
        <script>
        jQuery('.append-description').each(function(){
            var item = jQuery(this);
            var description = item.attr('item-description');
            item.parent().append('<div class="small_example"><small><i>'+description+'</i></small></div>');
        });
        </script>
        <?php
    }
    
    /**
     * Function: Validate Custom Fileds and Verify Age through API.
     */
    public function wcav_verify_age_validate(){
        
        $validate = 0;
        
        if ( ! $_POST['dob'] ){
            $validate = 1;
            wc_add_notice( __( '<b>Date of Birth</b> is a required field.' ), 'error' );
        }elseif (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_POST['dob'])) {
            $validate = 1;
            wc_add_notice( __( '<b>Date of Birth</b> is invalid. (Format: YYYY-MM-DD)' ), 'error' );
        }
        
        if ( ! $_POST['SSN'] ){
            $validate = 1;
            wc_add_notice( __( '<b>SSN#</b> is a required field.' ), 'error' );
        }elseif (!is_numeric($_POST['SSN'])) {
            $validate = 1;
            wc_add_notice( __( '<b>SSN#</b> is invalid. (Numbers Only)' ), 'error' );
        }elseif(strlen($_POST['SSN']) > 4){
            $validate = 1;
            wc_add_notice( __( '<b>SSN#</b> is invalid. (Only 4 Digit)' ), 'error' );
        }
        
        if($validate == 0){
            if($_POST['billing_first_name'] && $_POST['billing_last_name'] && $_POST['billing_address_1'] && $_POST['billing_state'] && $_POST['billing_postcode']){
                $StreetAddress=$_POST['billing_address_1'];
                if($_POST['billing_address_2']){
                    $StreetAddress=$StreetAddress." ".$_POST['billing_address_2'];
                }                
                if(!$this->wcav_IdentiFraud_Consumer_plus_api($_POST['billing_first_name'], $_POST['billing_last_name'], $StreetAddress, $_POST['billing_state'], $_POST['billing_postcode'], $_POST['dob'], $_POST['SSN'])){
                    wc_add_notice( __( 'Some of your inforamation could not be verified' ), 'error' );
                }                
            }
        }
    }
    
    public function wcav_IdentiFraud_Consumer_plus_api($FirstName,$LastName,$Street,$State,$ZipCode,$DateOfBirth,$SSN){
        // URL of the Consumer Product Service
        $serviceUrl = 'https://identiflo.everification.net/WebServices/Integrated/Main/V210/Consumer';
        // Username generated from platform site for use only with the web service
        $username = 'E897-D816180E-FEA8-4495-B864-604482E7F83D';
        // Password generated from platform site for use only with the web service
        $password = 'bzRN2XqztmbvV3e';

        // Identity information to search for
        $identity = array(
            'FirstName' => $FirstName,
            'LastName' => $LastName,
            'Street' => $Street,             
            'State' => $State,
            'ZipCode' => $ZipCode,
            'DateOfBirth' => $DateOfBirth,
            'SSN' => $SSN
        );

        // Build the XML Request
        $requestXml = '<?xml version="1.0" encoding="UTF-8"?>'.
            '<PlatformRequest>'.
                '<Credentials>'.
                    '<Username>'.$username.'</Username>'.
                    '<Password>'.$password.'</Password>'.
                '</Credentials>'.
                '<CustomerReference></CustomerReference>'.
                '<Identity>'.
                    '<FirstName>'.$identity['FirstName'].'</FirstName>'.
                    '<LastName>'.$identity['LastName'].'</LastName>'.
                    '<Ssn>'.$identity['SSN'].'</Ssn>'.
                    '<Street>'.$identity['Street'].'</Street>'.                                        
                    '<State>'.$identity['State'].'</State>'.
                    '<ZipCode>'.$identity['ZipCode'].'</ZipCode>'.
                    '<DateOfBirth>'.$identity['DateOfBirth'].'</DateOfBirth>'.
                '</Identity>'.
            '</PlatformRequest>';

        // Make the request
        $ch = curl_init($serviceUrl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);        

        $xml = new SimpleXMLElement($response);

        if($xml->Response){

            $AddressVerified=false;
            $SSNVerified=false;
            $DOBVerified=false;
            $ZipCodeVerified=false;

            $Address=$xml->Response->AddressVerificationResult;
            $SSN=$xml->Response->SocialSecurityNumberResult;
            $DOB=$xml->Response->DateOfBirthResult;
            $ZipCode=$xml->Response->StandardizedAddress->ZipCode;

            if(strpos(strtolower($Address),strtolower('Supplied full name matches supplied address')) !== false){                
                $AddressVerified=TRUE;
            }
            if(strpos(strtolower($SSN),strtolower('Match to full name and address using Social Security Number')) !== false){                
                $SSNVerified=TRUE;
            }
            if(strpos(strtolower($DOB),strtolower('Full DOB available and input matches exactly')) !== false){
                $DOBVerified=TRUE;
            }
            if($ZipCode == $ZipCode){
                $ZipCodeVerified=TRUE;
            }

            if($AddressVerified == TRUE && $SSNVerified == TRUE && $DOBVerified == TRUE && $ZipCodeVerified == TRUE){
                return TRUE;
            }else{
                return FALSE;                
            }
        }else{
            return FALSE;            
        }
    }
}