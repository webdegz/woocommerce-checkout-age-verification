<?php 
/*
Plugin Name: Wordpress Checkout Age Verification
Plugin URI: http://codup.io/
Description: Wordpress Checkout Age Verification with IdentiFraud Consumer+ API
Version: 1.0
Author Name: Codup
Author URI: http://codup.io/
*/
if (!defined('prefix')) { define("prefix", 'wcav_'); }
if (!defined('DS')) { define("DS", DIRECTORY_SEPARATOR); }
if (!defined('VERSION')) { define("VERSION", '1.0'); }

include(dirname(__FILE__) . DS . 'includes' . DS . 'class.wcav.php');
$WCAV = new WCAV();